Configurar y ejecutar localmente este proyecto de Symfony con PostgreSQL:

Requisitos Previos (el proyecto fue creado en la version 7 de Symfony)
PHP 8.1+
Composer
PostgreSQL 14+
Extensión PHP pgsql
Configuración Inicial

1. Clonar repo: 
    git clone https://gitlab.com/zorrillajuan1995/proyectoaaxis.git

2. Instalar dependencias de Composer:
    composer install

3. Crear base de datos en PostgreSQL:
    CREATE DATABASE AaxisTestDB;

4. Copiar archivo .env de ejemplo a .env.local y configurar credenciales de BD.

5. Ejecutar migraciones para crear tablas:
    php bin/console doctrine:migrations:migrate

6. Levantar el servidor Symfony local:
    symfony server:start
    
La aplicación ahora debería cargar correctamente apuntando a la base de datos configurada.

Testing API
Se puede probar los endpoints del API con Postman importando la colección incluida en /postman/coleccion.json.
