<?php

namespace App\Controller;

use App\Entity\ProyectoAaxis;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class AaxisController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    #[Route('/aaxis', name: 'app_aaxis')]
    public function index(): JsonResponse
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/AaxisController.php',
        ]);
    }

    /**
     * @Route("/api/products/load", name="api_products_load", methods={"POST"})
     */
    public function load(Request $request, User $user) { //receives JSON data, iterates over it, creates an entity for each item, persists them, and returns a JSON response with a success message.
        if(!$user) {
            return $this->json(['error' => 'Invalid User Token'], 401);
        }

        $data = json_decode($request->getContent(), true);
        
        if(empty($data)) {
            return $this->json(['error' => 'No data received'], 400); 
        }

        $entityManager = $this->getDoctrine()->getManager();  

        try {
            
            foreach ($data as $item) {        
                $product = new ProyectoAaxis(); 
                // mapeamos los datos del array a la entidad  
                $entityManager->persist($product);
            }

            $entityManager->flush();

            return $this->json(['message' => 'Producto Creado']);

        } catch (\Exception $e) {
            return $this->json(['error' => $e->getMessage()]);
        }
    }

    /**
    * @Route("/api/products/update", name="api_aaxis_update", methods={"PUT"})
    */
    public function update(Request $request) { //receives JSON data, iterates over it, finds the entity with the corresponding SKU, updates it, persists it, and returns a JSON response with the updated SKUs.

        $data = json_decode($request->getContent(), true);

        if(empty($data)) {
            return $this->json(['error' => 'No data sent'], 400);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $responseData = [];

        try {
            foreach ($data as $item) {
                
                $sku = $item['sku']; // get sku  
                $product = $this->getDoctrine()
                            ->getRepository(ProyectoAaxis::class)
                            ->findOneBy(['sku' => $sku]);

                if (!$product) {
                    $responseData['not_found_skus'][] = $sku;
                    continue; 
                }          

                // it can trigger the lifecycle events for some fields
                $product->update($item); 

                $entityManager->persist($product);

                $responseData['updated_products'][] = $product->getSku();
            }

            $entityManager->flush();

            return $this->json($responseData);

        } catch (\Exception $e) {
                
            return $this->json(['error' => $e->getMessage()]);
        }

    }
    /**
     * @Route("/api/products", name="api_products_list", methods={"GET"})
    */
    public function list() { //returns a JSON response with all the products.

        $products = $this->getDoctrine() 
                    ->getRepository(ProyectoAaxis::class) //We use Doctrine's repository to obtain all the products
                    ->findAll();
        
        return $this->json($products); 
    }

}
