<?php

namespace App\Entity;

use App\Repository\ProyectoAaxisRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ProyectoAaxisRepository::class)]
class ProyectoAaxis
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]

    private ?string $Id = null;

    #[ORM\Column(length: 255)]
    private ?string $incremental = null;

    #[ORM\Column(length: 50)]
    private ?string $Sku = null;

    #[ORM\Column(length: 250)]
    private ?string $Product_name = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $created_at = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $update_at = null;

    public function getId(): ?int
    {
        return $this->Id;
    }

    public function setId(string $Id): static
    {
        $this->Id = $Id;

        return $this;
    }

    public function getIncremental(): ?string
    {
        return $this->incremental;
    }

    public function setIncremental(string $incremental): static
    {
        $this->incremental = $incremental;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->Sku;
    }

    public function setSku(string $Sku): static
    {
        $this->Sku = $Sku;

        return $this;
    }

    public function getProductName(): ?string
    {
        return $this->Product_name;
    }

    public function setProductName(string $Product_name): static
    {
        $this->Product_name = $Product_name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeImmutable $created_at): static
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->update_at;
    }

    public function setUpdateAt(\DateTimeImmutable $update_at): static
    {
        $this->update_at = $update_at;

        return $this;
    }

    
}
