<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;


class TokenAuthListener
{

    public function onKernelController(ControllerEvent $event)
    {
        $user = $this->getUserFromToken();
        if (!$user) { //obtiene usuario desde token
            throw new UnauthorizedHttpException('Invalid token'); //If the user is not found, it throws an exception
        }
        $controller = $event->getController(); // Agregamos el usuario a los argumentos del controlador
        $controller[1]->setArgument('$user', $user);
    }

    private function getUserFromToken()
    {
        try {
            // implement logic to get user from token
            // return user or null if token is invalid
        } catch (Exception $e) {
            // handle any exceptions that occur during the process
            // log the error or perform any necessary actions
            return null;
        }
    }
}
