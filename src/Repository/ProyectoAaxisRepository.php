<?php

namespace App\Repository;

use App\Entity\ProyectoAaxis;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ProyectoAaxis>
 *
 * @method ProyectoAaxis|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProyectoAaxis|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProyectoAaxis[]    findAll()
 * @method ProyectoAaxis[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProyectoAaxisRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProyectoAaxis::class);
    }

//    /**
//     * @return ProyectoAaxis[] Returns an array of ProyectoAaxis objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?ProyectoAaxis
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
